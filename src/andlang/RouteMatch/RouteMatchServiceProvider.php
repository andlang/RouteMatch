<?php namespace andlang\RouteMatch;


use Illuminate\Support\ServiceProvider;

class RouteMatchServiceProvider extends ServiceProvider {

    /**
     * @var bool
     */
    protected $defer = false;

    /**
     * @return void
     */
    public function register()
    {
        $this->app['Router'] = $this->app->share(function($app)
        {
            $router = \App::make('andlang\RouteMatch\Router');
            return $router;
        });
    }

    /**
     * @return array
     */
    public function provides()
    {
        return array('router');
    }

}